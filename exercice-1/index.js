'use strict';
function gridGenerator(xAxis, yAxis) {
    const grid = document.createElement("table");
    for (let i = 0; i < yAxis; i++) {
        const row = grid.insertRow();
        for (let j = 0; j < xAxis; j++) {
            const cell = row.insertCell();
            const delay = Math.random() * 1000 + 1000; 
            setInterval(function() {
                cell.style.backgroundColor = getRandomColor();
            }, delay);
        }
    }
    return grid;
}

function getRandomColor() {
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

const grid = gridGenerator(10, 8);
document.body.appendChild(grid);