const grid = [
    [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 2, 2, 2, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0],
    [0, 3, 3, 3, 3, 0, 0, 0, 0, 0, 4, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 5, 5, 5, 5, 5, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
];

const history = [];

const refs = {
    1: {name: "Torpilleur", status: true, positions: []},
    2: {name: "Contre-torpilleurs", status: true, positions: []},
    3: {name: "Croiseur", status: true, positions: []},
    4: {name: "Porte-avions n°1", status: true, positions: []},
    5: {name: "Porte-avions n°2", status: true, positions: []}
}


function createBoard() {
    const table = document.createElement("table");
    for (let i = 0; i < grid.length; i++) {
        const row = document.createElement("tr");
        for (let j = 0; j < grid[i].length; j++) {
            const col = document.createElement("td")
            col.setAttribute("id", `box-${i}-${j}`);
            col.addEventListener("click", clickListener);
            // add positions to refs
            if (grid[i][j] != 0) {
                const count = refs[grid[i][j]].positions.push([i, j, true]);
                col.setAttribute("position-index", count-1);

            }
            row.appendChild(col);
        }
        table.appendChild(row);
    }
    drawRefs();
    return table;
}

function clickListener(event) {
    const row = event.target.id.split("-")[1];
    const col = event.target.id.split("-")[2];
    const hit = (grid[row][col] != 0);
    
    if (hit) {
        event.target.style.background = "red";
        const position = event.target.getAttribute('position-index');
        // kill position
        refs[grid[row][col]].positions[position][2] = false;
        checkHealthStatus(grid[row][col]);
    } else {
        event.target.style.background = "blue";
    }
    // add to history
    history.push([row, col, hit]); 
}

function checkHealthStatus(boatId) {
    for (let i = 0; i < refs[boatId].positions.length; i++) {
        if (refs[boatId].positions[i][2]) {
            return false;
        }
    }
    refs[boatId].status = false;    
    drawRefs();
    checkWin();
    return true;
}

function checkWin() {
    for (const key in refs) {
        if (refs[key].status) {
            return false;
        }
    }
    const refsContainer = document.getElementById('naval-refs');
    let h1 = document.createElement("h1");
    h1.innerHTML = "Vous avez gagné";
    refsContainer.appendChild(h1)
    return true;
}

function drawRefs() {
    const refsContainer = document.getElementById('naval-refs');
    refsContainer.innerHTML = "";

    const list = document.createElement("ul");
    for (const key in refs) {
        const element = refs[key];
        const li = document.createElement("li");
        var text = document.createTextNode(element.name);
        if (!element.status) {
            li.style.textDecoration = "line-through"
        }
        li.appendChild(text);
        list.appendChild(li);
    }
    refsContainer.appendChild(list);
}

const container = document.getElementById('naval-container');
container.appendChild(createBoard());

 